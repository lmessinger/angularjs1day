(function () {
    'use strict';

    //angular.module('1day', ['common', 'shell', 'home', 'help']);
    angular.module('1day', []);

    // RUN: App (module)
    angular
        .module('1day')
        .run(function ($rootScope) {

            $rootScope.safeApply = function (fn) {
                var phase = $rootScope.$$phase;
                if (phase === '$apply' || phase === '$digest') {
                    if (fn && (typeof (fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };

        });
        angular
            .module('1day').controller("endday",function($interval){
              var vm = this;

            });

            angular
                .module('1day').directive("myImagePresenter",function(){
                  return {
                    restrict: "E",
                    scope: {
                      titleColor:'@',
                      titleText:'='
                        },
                        controller: function($scope) {
                          console.log($scope.titleText)

                          $scope.$watch('titleText',function(n,o) {
                            console.log('change text',n,o)
                          })
                        },
                    templateUrl: "my-image-presenter-tpl.html"
                  }
                });


})();
