(function () {
    'use strict';

    //angular.module('1day', ['common', 'shell', 'home', 'help']);
    angular.module('1day', []);

    // RUN: App (module)
    angular
        .module('1day')
        .run(function ($rootScope) {

            $rootScope.safeApply = function (fn) {
                var phase = $rootScope.$$phase;
                if (phase === '$apply' || phase === '$digest') {
                    if (fn && (typeof (fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };

        });
        angular
            .module('1day').controller("endday",function($interval){
              var vm = this;
              vm.counter = 1;
              var intPromise = $interval(function() {
                if (!(--vm.counter)) {
                  vm.image = "/images/endoftheworld.png";
                  $interval.cancel(intPromise);
                }
              },1000)
            });

            angular
                .module('1day').directive("myImagePresenter",function(){
                  return {
                    restrict: "E",
                    // replace: true,
                    // scope: {
                    //           imgSrc: '='},
                    templateUrl: "my-image-presenter-tpl.html"
                  }
                });


})();
